function y = Horner(Poly, x)
    y = 0;
    for j = 1:length(Poly)
        y = y * x + Poly(j);
    end
end

