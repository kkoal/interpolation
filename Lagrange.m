function [ Poly, PolyParts ] = Lagrange( Xv, Yv )
    
    Xv = Xv';
    Yv = Yv';
    
    n = length(Xv);

    PolyParts = zeros(n,n);

    for j = 1:n
     
      CharPoly = poly(Xv( (1:n) ~= j));    
      PolyParts(j,:) = CharPoly ./ Horner(CharPoly, Xv(j));
      
    end

    Poly = Yv * PolyParts;

end

