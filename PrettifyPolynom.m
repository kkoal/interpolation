function PrettyPolynom = PrettifyPolynom(P)

    P = P(end:-1:1);
    Output = ''; 
    n = length(P);
    
    for j = n:-1:1
        if P(j) ~= 0
            if j > 2
                b = strcat('x', '^');
                b = strcat(b, num2str( j - 1 ));
            elseif j == 2
                b = 'x';
            else
                b = '';
            end
            if P(j) ~= 1 &&  P(j) ~= -1
                if strcmp(b, '') ~= 1
                    b=strcat('*', b);
                end
                b = strtrim(strcat(rats(abs(P(j))),b));
            else
                if j == 1; b = strtrim(strcat(rats(abs(P(j))))); end
            end
            if strcmp(Output, '') == 1
                Output = [Output ' ' b ' '];
                if P(j) < 0; Output = ['-' Output]; end
            else
                if P(j) < 0; Output = [Output ' ' '-'];
                else; Output = [Output ' ' '+']; end
                Output = [Output ' ' b ' '];
            end
        end
    end
    
PrettyPolynom = Output;

end