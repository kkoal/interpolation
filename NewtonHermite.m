function [Poly,PolyParts] = NewtonHermite(Xv,Yv)

    n = length(Xv);
    DivDiffs = zeros(n);
    PolyParts = zeros(n,n);
    Poly = zeros(1,n);

    z = arrayfun(@(i) Yv(find(Xv == Xv(i), 1 )), 1:n);

    for j = n:-1:1
        for k = j:n
            if j == k
                DivDiffs(j,k) = z(j);
            elseif Xv(j) == Xv(k)
                sI = find(Xv == Xv(j), 1 );
                DivDiffs(j, k) = Yv(k - j + sI) / factorial(k - j);
            else
                DivDiffs(j, k) = (DivDiffs(j + 1, k) - DivDiffs(j, k - 1)) / (Xv(k) - Xv(j));
            end;
        end;
    end;
    
    for j = 1:n
        PolyParts(j,:) = [zeros(1,n-j), conv(DivDiffs(1,j),poly(Xv(1:j-1)))];
        Poly = Poly + PolyParts(j,:);
    end 
    
end